
# run phonome
$ g2p-seq2seq --interactive --model_dir g2p-seq2seq-model-6.2-cmudict-nostress
$ g2p-seq2seq --decode word_list --model_dir g2p-seq2seq-model-6.2-cmudict-nostress --output word_list_out

# short keys
$ g2p_besic
$ g2p_decode

# create file .voca .grammar

# generate .dict .dfa .term
$ rosrun tmc_rosjulius mkdfa.sh grammar

# create .txt 

# copy dir to HSR
$ scp -r grammar/ dongyang@hsrb.local:/home/dongyang/Documents
$ cd /etc/opt/tmc/robot/conf.d/dics
$ sudo scp -r dongyang@hsrb.local:/home/dongyang/Documents/word .
$ sudo scp -r dongyang@hsrb.local:/home/dongyang/Documents/grammar .
$ sudo cp -a grammar/. .

# set param in .yalm
$ cd /etc/opt/tmc/robot
$ sudo vim params.yaml

# check status dictionary
$ rosservice call /hsrb/voice/list_dictionaries '{with_refresh: False}'

# add dictionary but param must already set
$ rosservice call /hsrb/voice/add_dictionary 1 1 gramsim '[]' "/etc/opt/tmc/robot/conf.d/dics/gramsim"
$ rosservice call /hsrb/voice/add_dictionary 0 1 wfw '[]' ""

$ rosservice call /hsrb/voice/activate_dictionaries '{names: [word_sample], active: False}'


rosservice call /hsrb/voice/add_dictionary 0 1 fruit_word '[]' ""
rosservice call /hsrb/voice/activate_dictionaries '{names: [word_sample], active: True}'

# STEP
# don't forget to run simulation bottom on Gazebo !!!

sim_run = roslaunch hsrb_gazebo_launch hsrb_empty_world.launch use_perception:=false fast_physics:=true

sim_mode

roslaunch tmc_rosjulius speech_recognition.launch lm_locale:='en' lm_grammar_mode:=True dic_list:=/home/aufa/dics/grammar_en/dic_list.txt

rosservice call /hsrb/voice/list_dictionaries '{with_refresh: False}'

rostopic echo /hsrb/voice/status

rostopic echo /hsrb/voice/text

rostopic hz /hsrb/voice/text



##### >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

#export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
#export PATH=$PATH:/usr/lib/jvm/java-8-openjdk-amd64/bin

source /opt/ros/kinetic/setup.bash
source /home/aufa/ws_moveit_build/devel/setup.bash
source /home/aufa/catkin_ws/devel/setup.bash

# please set network-interface

network_check=0
network_if=wlp3s0 
export TARGET_IP=$(LANG=C /sbin/ifconfig $network_if | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*')
if [ -z "$TARGET_IP" ] ; then
    #echo "ROS_IP is not set."
	network_check=1
else
    export ROS_IP=$TARGET_IP
    #echo $TARGET_IP
	echo "Wireless connected"
fi
if [ $network_check == 1 ] ; then
	network_if=enp4s0
	export TARGET_IP=$(LANG=C /sbin/ifconfig $network_if | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*')
	if [ -z "$TARGET_IP" ] ; then
    	echo "ROS_IP is not set."
	else
    	export ROS_IP=$TARGET_IP
    	#echo $TARGET_IP
		echo "Ethernet connected"
	fi
fi

export ROS_HOME=~/.ros
alias sim_mode='export ROS_MASTER_URI=http://localhost:11311 export PS1="\[\033[44;1;37m\]<local>\[\033[0m\]\w$ "'
alias hsrb_mode='export ROS_MASTER_URI=http://hsrb.local:11311 export PS1="\[\033[41;1;37m\]<hsrb>\[\033[0m\]\w$ "'

alias admin_hsr='ssh administrator@hsrb.local'
alias dongyang_hsr='ssh dongyang@hsrb.local'

alias venv='source ~/tensorflow/venv/bin/activate'
alias g2p_besic='g2p-seq2seq --interactive --model_dir g2p-seq2seq-model-6.2-cmudict-nostress'
alias g2p_decode='g2p-seq2seq --decode word_list --model_dir g2p-seq2seq-model-6.2-cmudict-nostress --output word_list_out'

alias whitePC_get='export ROS_MASTER_URI=http://192.168.0.108:11311 export ROS_HOSTNAME=192.168.0.102'

# For simulation hot command
alias sim_run='roslaunch hsrb_gazebo_launch hsrb_empty_world.launch use_perception:=false fast_physics:=true'
alias serdic="rosservice call /hsrb/voice/list_dictionaries '{with_refresh: False}'"
alias text='rostopic echo /hsrb/voice/text'
alias status='rostopic echo /hsrb/voice/status'
alias speechword='roslaunch tmc_rosjulius speech_recognition.launch dic_list:=/home/aufa/dics/word_sample_en/dic_list.txt'
alias speechgrammar='roslaunch tmc_rosjulius speech_recognition.launch lm_grammar_mode:=True dic_list:=/home/aufa/dics/grammar_en/dic_list.txt timeout:=10'

# Thesis hot command
alias qrun="terminator -m --layout=ironwolf"

export PATH=$PATH:/home/aufa/mixcell/QTCode
export PYTHONPATH=$PYTHONPATH:/home/aufa/mixcell/source_code

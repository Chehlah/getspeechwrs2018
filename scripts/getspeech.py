#!/usr/bin/env python

# Test publish msg
# >> rostopic pub /hsrb/voice/text tmc_rosjulius_msgs/RecognitionResult (and tap2)
# Generate command

import rospy, os, sys
from tmc_rosjulius_msgs.msg import RecognitionResult
from std_msgs.msg import String

gettext = RecognitionResult()

global ask_check, b_check, t_check, a_check, pub
ask_check = 0 # no check
b_check = 0
t_check = 0
a_check = 0

def targetCheck(text):
	global ask_check, b_check, t_check, a_check, pub

	# print("robot is checking")
	text_ = text.split()

	for word in text_:
		# print word
		if word == "bring":
			b_check = b_check  + 1 
		elif word == "me":
			b_check = b_check  + 1 
		elif word == "tidy":
			t_check = t_check + 1
		elif word == "up":
			t_check = t_check + 1
		elif word == "yes":
			a_check = 1
		elif word == "no":
			a_check = -1
		
	if ask_check == 1 and a_check == -1:
		print "what do you want?" 
		a_check = 0
		ask_check = 0
	elif ask_check == 1 and a_check == 1 and b_check == 2: 
		print "I am going to do 'Bring me'"
		b_check = 0
		a_check = 0
		ask_check = 0
		pub.publish("bring_me")
	elif ask_check == 1 and a_check == 1 and t_check == 2:
		print "I am going to do 'tidy up'" 
		t_check = 0
		a_check = 0
		ask_check = 0
		pub.publish("tidy_up")
	elif b_check == 2:
		print "Do you want me to do 'Bring me'"
		# >>>> speak out
		# >>>> save target first
		ask_check = 1
	elif t_check == 2:
		print "Do you want me to do 'tidy up'"
		# >>>> speak out
		# >>>> save target first
		ask_check = 1	

	if ask_check == 0:
		print "waiting next command"


def callback(data):
	gettext = data
	#print(gettext.sentences[0])

	# string check and split
	inText = gettext.sentences[0]
	# print(inText)
	# string type
	# print(type(inText))

	targetCheck(inText)


def getspeech():
	global pub

	rospy.init_node('getspeech', anonymous=True)
	# Topic from HSR > "/hsrb/voice/text"
	# text msg type > tmc_rosjulius_msgs/RecognitionResult
	rospy.Subscriber("/hsrb/voice/text", RecognitionResult, callback)
	pub = rospy.Publisher("/main/QR", String, queue_size = 10)

	rospy.spin()

if __name__ == '__main__':
	try:
		getspeech()
	except rospy.ROSInterruptException: pass
#!/usr/bin/env python

# Speech for Tidy Up WRS2018
# Author: Aufa 
# Last edit: 17:01 , 16/10/2018
# Place: Hall-7, Tokyo Big Size, Tokyo, Japan.

# Test get input text_msg
# rostopic pub /hsrb/voice/text tmc_rosjulius_msgs/RecognitionResult (and tap2)


import rospy, os, sys
from tmc_rosjulius_msgs.msg import RecognitionResult
from std_msgs.msg import String
from tmc_msgs.msg import Voice

global ask_check, b_check, t_check, a_check
global pub, pub_speakout
ask_check = 0 # no check
b_check = 0
t_check = 0
a_check = 0

gettext = RecognitionResult()
speakout = Voice()
speakout.language = 1
speakout.interrupting = False
speakout.queueing = False
'''
	int32 kJapanese=0
	int32 kEnglish=1
	bool interrupting
	bool queueing
	int32 language
	string sentence
'''

def targetCheck(text):
	global ask_check, b_check, t_check, a_check 
	global pub, pub_speakout, speakout

	# print("robot is checking")
	text_ = text.split()

	if ask_check == 0:
		for word in text_:
			# print word
			if word == "tidy":
				t_check = t_check + 1
			elif word == "up":
				t_check = t_check + 1
	elif ask_check == 1:
		for word in text_:
			if word == "yes":
				a_check = 1
			elif word == "no":
				a_check = -1
		
	if ask_check == 1 and a_check == -1:
		print "You say NO what do you want me to do?" 
		speakout.sentence = "You say NO what do you want me to do"
		pub_speakout.publish(speakout)
		a_check = 0
		ask_check = 0
	elif ask_check == 1 and a_check == 1 and t_check == 2:
		print "I am going to do 'tidy up'" 
		speakout.sentence = "I am going to do tidy up"
		pub_speakout.publish(speakout)
		t_check = 0
		a_check = 0
		ask_check = 3
		pub.publish(">> start") 
		print "Already get command"
	elif t_check == 2:
		print "Do you want me to do 'tidy up'"
		speakout.sentence = "Do you want me to do tidy up"
		pub_speakout.publish(speakout)
		# >>>> speak out
		# >>>> save target first
		ask_check = 1	


def callback(data):
	gettext = data
	#print(gettext.sentences[0])

	# string check and split
	inText = gettext.sentences[0]
	# print(inText)
	# string type
	# print(type(inText))

	targetCheck(inText)


def getspeech():
	global pub, pub_speakout

	rospy.init_node('Tidy_up', anonymous=True)
	# Topic from HSR > "/hsrb/voice/text"
	# text msg type > tmc_rosjulius_msgs/RecognitionResult
	rospy.Subscriber("/hsrb/voice/text", RecognitionResult, callback)
	pub = rospy.Publisher("/main/QR_code", String, queue_size = 10)
	pub_speakout = rospy.Publisher("/talk_request", Voice, queue_size = 10)

	rospy.spin()

if __name__ == '__main__':
	try:
		getspeech()
	except rospy.ROSInterruptException: pass
#!/usr/bin/env python

# Speech for Bring me WRS2018
# Author: Aufa 
# Last edit: 10:07 , 19/10/2018
# Place: Hall-7, Tokyo Big Size, Tokyo, Japan.

# Test get input text_msg
# rostopic pub /hsrb/voice/text tmc_rosjulius_msgs/RecognitionResult (and tap2)


import rospy, os, sys
from tmc_rosjulius_msgs.msg import RecognitionResult
from std_msgs.msg import String
from tmc_msgs.msg import Voice
#from getspeech.msg import bring

global pub, pub_speakout, speakout
gettext = RecognitionResult()
speakout = Voice()
speakout.language = 1
speakout.interrupting = False
speakout.queueing = False
# speakout.sentence = "String"
# pub_speakout.publish(speakout)
'''
	int32 kJapanese=0
	int32 kEnglish=1
	bool interrupting
	bool queueing
	int32 language
	string sentence
'''

# dictionary name list
global nameList, text_ask, ask_check, target_pub, target_num
text_ask = ''
ask_check = 0
target_pub = ['','','','']
target_num = 0
nameList = {
	'wooden bowl'		:'wooden bowl',
	'bowl pink'			:'pink bowl',
	'blue bowl'			:'blue bowl',
	'blue cup'			:'blue cup',
	'pink cup'			:'pink cup',
	'orange biscuits'	:'orange biscuits',
	'yellow biscuits'	:'yellow biscuits',
	'pink biscuits'		:'pink biscuits',
	'dish green'		:'green dish',
	'dish yellow'		:'yellow dish',
	'orange drink'		:'orange drink',
	'green drink'		:'green drink',
	'blue berry'		:'blueberry drink',
	'can'				:'watering can',
	'foil'				:'aluminum foil',
	'mustard'			:'canned mustard',
	'plant'				:'plant',
	'pail'				:'brown pail',
	'nut'				:'mixed nuts',
	'fork'				:'blue fork',
	'spoon'				:'green spoon',
	'knife'				:'gray knife',
	'clock'				:'yellow clock',
	'tea'				:'oolong tea',
	'water'				:'mineral water',
	'bottle'			:'water bottle',
	'light'				:'flashlight',
	'tomato'			:'tomato',
	'potato'			:'potato',
	'spray'				:'spray',
	'ket chup'			:'ketchup'
	}	

def targetCheck(text):
	global pub, pub_speakout, speakout, ask_check
	text = text.split()
	print (text)
	if ask_check == 0:
		for word in text:
			if word == 'start':
				pub.publish("HSR START")
				ask_check = 1
				speakout.sentence = 'i am going to do bring me'
				pub_speakout.publish(speakout)
				break

'''
def targetCheck(text):
	global nameList, text_ask, ask_check, target_pub, target_num
	global pub, pub_speakout, speakout
	#text = text.split()
	#print ('{}'.format(text[0]))

	# check verb
	text_check = text.split()
	for check in text_check:
		if check == 'take' or check == 'bring':
			text = text_check[1]

	# ask yes or no
	if ask_check == 1:
		for word in text_check:
			if word == "robot":
				if text_check[1] == 'yes':
					# pubhere or save data for send
					print ('You say Yes >> {}'.format(text_ask))
					# speak out
					sum_string = 'you say yes '+text_ask
					speakout.sentence = sum_string
					pub_speakout.publish(speakout)
					ask_check = 0
					target_pub[target_num] = text_ask
					target_num = target_num + 1 
					break
				elif text_check[1] == 'no':
					text_ask = ''
					print ('What do you want')
					# speak out	
					speakout.sentence = 'what do you want'
					pub_speakout.publish(speakout)
					ask_check = 0
					break
	if ask_check == 1:
		print ("Yes or No")
		# speak out
		speakout.sentence = 'yes or no'
		pub_speakout.publish(speakout)

	# search the full name
	if ask_check == 0:
		for word in nameList:
			if word == text:
				print ('Do you want {}'.format(nameList[text]))
				text_ask = nameList[text]
				# speak out
				sum_string = 'do you want '+text_ask
				speakout.sentence = sum_string
				pub_speakout.publish(speakout)
				ask_check = 1
				break
	
	# save data 1-4 and send to main	
	if target_num == 4:
		# pub all data to main here
		print (target_pub)
		pub.publish("HSR start")
		# speak out
		sum_string = 'i already send all target'
		speakout.sentence = sum_string
		pub_speakout.publish(speakout)
		ask_check = 2
		target_num = 0	
'''

def callback(data):
	gettext = data
	#print(gettext.sentences[0])

	# string check and split
	inText = gettext.sentences[0]
	# print(inText)
	
	# string type
	# print(type(inText))
	
	targetCheck(inText)


def getspeech():
	global pub, pub_speakout

	rospy.init_node('Bring_me', anonymous=True)
	# Topic from HSR > "/hsrb/voice/text"
	# text msg type > tmc_rosjulius_msgs/RecognitionResult
	rospy.Subscriber("/hsrb/voice/text", RecognitionResult, callback)
	pub = rospy.Publisher("/main/QR_code", String, queue_size = 10)
	pub_speakout = rospy.Publisher("/talk_request", Voice, queue_size = 10)

	rospy.spin()

if __name__ == '__main__':
	try:
		getspeech()
	except rospy.ROSInterruptException: pass